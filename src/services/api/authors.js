import { query, mutate } from '../graphQL';

export function getAuthors() {
  const ggl = `
    query getAuthors {
      authors {
        id,
        firstName,
        lastName,
        created,
        books {
          id,
          name
        }
      }
    }
  `;

  return query(ggl)
    .then(result => ({ data: result.data.authors.map(a => ({ ...a, books: a.books.map(b => b.id) })) }));
}

export function removeAuthor(id) {
  const gql = `
    mutation removeAuthor($id: ID!) {
      removeAuthor(id: $id) {
        id
      }
    }
  `;

  return mutate(gql, { id }).then(result => ({ data: result.data.removeAuthor }));
}

export function updateAuthor(data) {
  const gql = `
    mutation updateAuthor($id: ID!, $firstName: String, $lastName: String, $books: [ID!]) {
      updateAuthor(input: { id: $id, firstName: $firstName, lastName: $lastName, books: $books }) {
        id,
        firstName,
        lastName,
        created,
        books {
          id,
          name
        }
      }
    }
  `;

  return mutate(gql, data).then(result => ({ data: result.data.updateAuthor }));
}

export function createAuthor(data) {
  const gql = `
    mutation createAuthor($firstName: String!, $lastName: String!, $books: [ID!]) {
      createAuthor(input: { firstName: $firstName, lastName: $lastName, books: $books }) {
        id,
        firstName,
        lastName,
        created,
        books {
          id,
          name
        }
      }
    }
  `;

  return mutate(gql, data)
    .then(result => ({ data: result.data.createAuthor }));
}
