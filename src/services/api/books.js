import { query, mutate } from '../graphQL';

export function getBooks() {
  const ggl = `
    query getBooks {
      books {
        id,
        name,
        created,
        author {
          id,
          firstName,
          lastName,
          created
        },
        genre
      }
    }
  `;

  return query(ggl).then(result => ({ data: result.data.books }));
}

export function getBook(id) {
  const ggl = `
    query getBook($id: ID!) {
      book(id: $id) {
        id,
        name,
        created,
        author {
          id,
          firstName,
          lastName,
          created
        },
        genre
      }
    }
  `;

  return query(ggl, { id }).then(result => ({ data: result.data.book }));
}

export function removeBook(id) {
  const gql = `
    mutation removeBook($id: ID!) {
      removeBook(id: $id) {
        id
      }
    }
  `;

  return mutate(gql, { id }).then(result => ({ data: result.data.removeBook }));
}

export function updateBook(data) {
  delete data.author;

  const gql = `
    mutation updateBook($id: ID!, $name: String, $author: ID, $genre: Genre) {
      updateBook(input: { id: $id, name: $name, author: $author, genre: $genre }) {
        id,
        name,
        created,
        author {
          id,
          firstName,
          lastName,
          created
        },
        genre
      }
    }
  `;

  return mutate(gql, data).then(result => ({ data: result.data.updateBook }));
}

export function createBook(data) {
  const gql = `
    mutation createBook($name: String!, $author: ID!, $genre: Genre) {
      createBook(input: { name: $name, author: $author, genre: $genre }) {
        id,
        name,
        created,
        author {
          id,
          firstName,
          lastName,
          created
        },
        genre
      }
    }
  `;

  return mutate(gql, { ...data, author: '5d233dd025940833f563b4d1' }) // TODO: Use real id fpr author
    .then(result => ({ data: result.data.createBook }));
}

// TODO: Add subscription on new book
