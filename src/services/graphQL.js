import ApolloClient from 'apollo-boost';
import gql from 'graphql-tag';

const client = new ApolloClient({
  uri: 'http://localhost:3003/graphql'
});

export const query = (query, variables) => client.query({query: gql(query), variables});
export const mutate = (mutation, variables) => client.mutate({mutation: gql(mutation), variables});

export default client;
