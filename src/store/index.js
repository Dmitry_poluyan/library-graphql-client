import Vue from 'vue';
import Vuex from 'vuex';

import books from './modules/books';
import authors from './modules/authors';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    books, // TODO: Upper case of name
    authors
  },
  strict: debug
});
